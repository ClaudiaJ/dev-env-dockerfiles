# Build

`docker build --build-arg SCHEME=full -t texlive-full:2017 .`

# Run

```sh
docker run --rm -it \
    --name texlive-full-2017 \
    -v $PWD:/var/build \
    texlive-full:2017
```
