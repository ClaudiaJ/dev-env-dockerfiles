#!/bin/sh

# Try to use the same user and group as the volume directory
export WORKDIR=/var/build
USER_ID=$(stat -c "%u" $WORKDIR)
GROUP_ID=$(stat -c "%g" $WORKDIR)
echo "Starting as user identified by $USER_ID:$GROUP_ID"
if [ -z $USER_ID ] || [ -z $GROUP_ID ]; then
    echo "Couldn't get USER_ID or GROUP_ID from $WORKDIR"
    exit 1
fi

# Do not attempt to create root id
if [ $USER_ID -ne 0 ] && [ $GROUP_ID -ne 0 ]; then
    # Verify no group exists and can successfully create group
    if groupadd -f -g $GROUP_ID texusers
    then echo "Created group texusers with GID $GROUP_ID"
    else
        echo "Could not create or use group with GID $GROUP_ID"
        exit 1
    fi

    # Verify no user exists and can successfully create user
    if ! getent passwd $USER_ID \
        && useradd -o -s /bin/bash -u $USER_ID -d /home/texuser -c "" -g $GROUP_ID texuser
    then echo "Created user texuser with UID $USER_ID"
    else
        echo "Could not create user with UID $USER_ID"
        exit 1
    fi
else
    echo "ERROR: Do not run as root. Make sure to mount a workdir volume over $WORKDIR"
    exit 1
fi

exec /usr/sbin/gosu texuser "$@"
